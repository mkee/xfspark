package com.troila.xfspark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

@EnableConfigurationProperties
@EnableWebSocket
@SpringBootApplication
public class XfsparkApplication {

    public static void main(String[] args) {
        SpringApplication.run(XfsparkApplication.class, args);
    }

}
