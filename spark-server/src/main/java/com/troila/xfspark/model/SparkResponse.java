package com.troila.xfspark.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SparkResponse {
    private SparkHeader header;
    private SparkPayload payload;

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Data
    public static class SparkHeader {
        private Integer code;
        private String message;
        private Integer status;
        private String sid;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Data
    public static class SparkPayload {
        private SparkChoices choices;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Data
    public static class SparkChoices {
        private Integer status;
        private Integer seq;
        private List<SparkRoleContent> text;
    }

}
