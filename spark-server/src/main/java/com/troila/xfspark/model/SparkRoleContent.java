package com.troila.xfspark.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SparkRoleContent {

    @Builder.Default
    private String role = "user";

    private String content;

    private Integer index;
}
