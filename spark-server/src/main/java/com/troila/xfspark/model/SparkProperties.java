package com.troila.xfspark.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
@ConfigurationProperties(prefix = "xf.spark")
public class SparkProperties {

    private String appId;
    private String apiSecret;
    private String apiKey;
    private String apiUrl;
}
