package com.troila.xfspark.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SparkRequest {

    private SparkHeader header;

    @Builder.Default
    private SparkParameter parameter = SparkParameter.builder().build();

    private SparkPayload payload;

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Data
    public static class SparkHeader {
        @JsonProperty("app_id")
        private String appId;

        @JsonProperty("uid")
        private String userId;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Data
    public static class SparkParameter {
        @Builder.Default
        private SparkChat chat = SparkChat.builder().build();
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Data
    public static class SparkChat {

        @Builder.Default
        private String domain = "generalv2";

        @Builder.Default
        private Double temperature = 0.5;

        @Builder.Default
        @JsonProperty("max_tokens")
        private Integer maxTokens = 8192;

        @Builder.Default
        @JsonProperty("top_k")
        private Integer topP = 4;

        @JsonProperty("chat_id")
        private String chatId;

    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Data
    public static class SparkPayload {
        private SparkMessage message;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Data
    public static class SparkMessage {
        private List<SparkRoleContent> text;
    }
}
