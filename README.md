# 讯飞星火大模型示例

#### 项目介绍
springboot + websocket + react + antd + 星火大模型API 

#### 软件架构
采用前后端分离的设计模式，前端使用 react + antd 做 websocket 客户端，后端使用 springboot 提供 websocket 服务端，同时调用星火大模型的 API 做数据交互。 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
