#### 1.使用 typescript 模板创建 react 项目
```shell
npx create-react-app spark-front --template typescript
```
#### 2.安装 Ant Design
```shell
cd spark-front
npm install antd --save-dev
```
#### 3.安装 http-proxy-middleware
```shell
npm install http-proxy-middleware --save-dev
```
#### 4.安装 reconnecting-websocket
```shell
npm install reconnecting-websocket --save-dev
```